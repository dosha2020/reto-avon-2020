<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('/'); 
Route::get('/terminos-y-condiciones', 'FrontController@terms')->name('terminos-y-condiciones');
Route::get('/login', 'Auth/LoginController@terms')->name('login');
Route::get('/juego-gato', 'FrontController@gamecat')->name('juego-gato');
Route::get('/juego-memoria', 'FrontController@gamememory')->name('juego-memoria');
Route::get('/ganadores', 'FrontController@winners')->name('winners');
Route::get('/gracias', 'FrontController@comingSoon')->name('comingSoon');

Auth::routes();

Route::get('/competition', 'HomeController@index')->name('competition');
