/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app3.js":
/*!******************************!*\
  !*** ./resources/js/app3.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

// declare modal
var modal = document.getElementById("popup1");
var modal2 = document.getElementById("popup2");
var modal3 = document.getElementById("popup3"); // close icon in modal

var closeicon = document.querySelector(".close");
var closeicon2 = document.querySelector(".close2");
var closeicon3 = document.querySelector(".close3");
var isX = true;
var isValid = false;
var cells = [];
var player = 1;
var computer = 0;
var onMove = 1;
var result = "";
var intervalId;
$(document).ready(function () {
  $(".option").bind("click", function () {
    isX = $(this).data("option") == "true" ? true : false;
    startGame();
  });
});

function startGame() {
  emptyCells();

  if (isX) {
    player = 1;
    computer = 0;
  } else {
    player = 0;
    computer = 1;
  }

  var rnd = Math.round(Math.random());

  if (rnd === 1) {
    onMove = player;
  } else {
    onMove = computer;
  }

  intervalId = setInterval(loop, 100);
}

function loop() {
  if (onMove === player) {
    isValid = true;
  } else {
    computerMove();
    onMove = player;
  }

  $(".cell").on("click", function () {
    if (isValid) {
      var sign = player === 0 ? "O" : "X";
      var i = $(this).attr("id")[1] - 1;
      var j = $(this).attr("id")[2] - 1;

      if (cells[i][j] === -1) {
        cells[i][j] = player;
        $(this).html(sign);
        isValid = false;

        if (!whoWon(cells)) {
          onMove = computer;
        }
      }
    }
  });
  var winner = whoWon(cells);

  if (winner) {
    if (winner === "computer") {
      modal.classList.add("show");
      closeModal(); //     alert("You Lost, Better Luck Next Time");
    } else {
      modal2.classList.add("show2");
      closeModal2(); //   alert("Congratulation!! You Won");
    }

    result = winner;
  } else if (isTableFull(cells)) {
    modal3.classList.add("show3");
    result = "Draw";
    closeModal3(); //    alert("Match Is "+ result);
  }

  if (result !== "") {
    clearInterval(intervalId);
    result = "";
    cells = [];
    clearTable();
    startGame();
  }
} //findel loop


function isTableFull(cells) {
  for (var i in cells) {
    for (var j in cells[i]) {
      if (cells[i][j] === -1) {
        return false;
      }
    }
  }

  return true;
}

function whoWon(cells) {
  for (var i in cells) {
    if (cells[i][0] !== -1 && cells[i][1] !== -1 && cells[i][2] !== -1 && cells[i][0] === cells[i][1] && cells[i][0] === cells[i][2]) {
      if (cells[i][0] === player) {
        return "player";
      } else {
        return "computer";
      }
    }
  }

  for (var i in cells) {
    if (cells[0][i] !== -1 && cells[1][i] !== -1 && cells[2][i] !== -1 && cells[0][i] === cells[1][i] && cells[0][i] === cells[2][i]) {
      if (cells[0][i] === player) {
        return "player";
      } else {
        return "computer";
      }
    }
  }

  if (cells[0][0] !== -1 && cells[1][1] !== -1 && cells[2][2] !== -1 && cells[0][0] === cells[1][1] && cells[0][0] === cells[2][2] || cells[0][2] !== -1 && cells[1][1] !== -1 && cells[2][0] !== -1 && cells[0][2] === cells[1][1] && cells[0][2] === cells[2][0]) {
    if (cells[1][1] === player) {
      return "player";
    } else {
      return "computer";
    }
  }

  return false;
}

function computerMove() {
  var arr = freeCells();
  var arr2 = bestMove();
  var sign = computer === 0 ? "O" : "X";
  var x = Math.round(Math.random() * arr.length);

  if (arr2.length >= 1) {
    var i = arr2[0];
    var j = arr2[1];
  } else if (arr.length >= 1) {
    var i = arr[x][0];
    var j = arr[x][1];
  }

  if ((arr.length >= 1 || arr2.length >= 1) && cells[i][j] === -1) {
    var a = parseInt(i) + 1;
    var b = parseInt(j) + 1;
    new Promise(function (resolve, reject) {
      $("#c" + a + b).html(sign);
      resolve();
    }).then(function () {
      cells[i++][j++] = computer;
    });
    return 0;
  }
}

function freeCells() {
  var arr = [];

  for (var i in cells) {
    for (var j in cells[i]) {
      if (cells[i][j] === -1) {
        arr.push([i, j]);
      }
    }
  }

  return arr;
}

function bestMove() {
  var computerInLine = 0;
  var playerInLine = 0;
  var a = 0;
  var maybe = null; //check rows

  for (var i in cells) {
    for (var j in cells[i]) {
      if (cells[i][j] !== -1) {
        if (cells[i][j] === computer) {
          computerInLine++;
        } else {
          playerInLine++;
        }
      } else {
        a = j;
      }
    }

    if (computerInLine === 2 && a !== 0) {
      return [i, a];
    } else if (playerInLine === 2 && a !== 0) {
      maybe = [i, a];
    }

    a = 0;
    computerInLine = 0;
    playerInLine = 0;
  } //check columns


  for (var j in cells) {
    for (var i in cells[i]) {
      if (cells[i][j] !== -1) {
        if (cells[i][j] === computer) {
          computerInLine++;
        } else {
          playerInLine++;
        }
      } else {
        a = i;
      }
    }

    if (computerInLine === 2 && a !== 0) {
      return [a, j];
    } else if (playerInLine === 2 && a !== 0) {
      maybe = [a, j];
    }

    a = 0;
    computerInLine = 0;
    playerInLine = 0;
  } //check diagonals


  var h = 0;
  a = -1;
  var b = -1;
  playerInLine = 0;
  computerInLine = 0;

  for (var k = 0; k < 3; k++) {
    if (cells[k][h] !== -1) {
      if (cells[k][h] === computer) {
        computerInLine++;
      } else {
        playerInLine++;
      }
    } else {
      a = k;
      b = h;
    }

    if (computerInLine === 2 && a !== -1 && b !== -1) {
      return [a, b];
    } else if (playerInLine === 2 && a !== -1 && b != -1) {
      maybe = [a, b];
    }

    h++;
  }

  h = 0;
  a = -1;
  b = -1;
  playerInLine = 0;
  computerInLine = 0;

  for (var k = 2; k >= 0; k--) {
    if (cells[k][h] !== -1) {
      if (cells[k][h] === computer) {
        computerInLine++;
      } else {
        playerInLine++;
      }
    } else {
      a = k;
      b = h;
    }

    if (computerInLine === 2 && a !== -1 && b !== -1) {
      return [a, b];
    } else if (playerInLine === 2 && a !== -1 && b != -1) {
      maybe = [a, b];
    }

    h++;
  }

  if (maybe !== null) return maybe;
  return [];
}

function clearTable() {
  for (var a = 1; a <= 3; a++) {
    for (var b = 1; b <= 3; b++) {
      $("#c" + a + b).html("");
    }
  }
}

function emptyCells() {
  for (var a = 0; a < 3; a++) {
    cells.push([-1, -1, -1]);
  }
}

function closeModal() {
  closeicon.addEventListener("click", function (e) {
    modal.classList.remove("show");
  });
}

function closeModal2() {
  closeicon2.addEventListener("click", function (e) {
    modal2.classList.remove("show2");
  });
}

function closeModal3() {
  closeicon3.addEventListener("click", function (e) {
    modal3.classList.remove("show3");
  });
}

/***/ }),

/***/ 2:
/*!************************************!*\
  !*** multi ./resources/js/app3.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Volumes/Peter/FLUX/reto-avon-2020/resources/js/app3.js */"./resources/js/app3.js");


/***/ })

/******/ });