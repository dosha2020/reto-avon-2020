const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/app.js')
	   .js('resources/js/app2.js', 'public/js/app2.js')
	   .js('resources/js/app3.js', 'public/js/app3.js')
       .sass('resources/sass/app.scss', 'public/css/app.css')
       .sass('resources/sass/memory.scss', 'public/css/app2.css')
       .sass('resources/sass/cat-game.scss', 'public/css/app3.css')
       .scripts([
            'resources/js/scripts/*.js',
        ], 'public/js/all.js')
	.copy('resources/img/*', 'public/img/')
	.version();
