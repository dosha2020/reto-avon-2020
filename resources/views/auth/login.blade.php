@extends('Front.mainlogin')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 colmd8-login">
            <div class="cardlogin">
                <div class="card-headerlogin">Iniciar sesión</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="noAfiliate" class="col-md-4 col-form-label text-md-right txtlabel">Número de afiliado</label>

                            <div class="col-md-6 labelinput">
                                <input id="noAfiliate" type="text" class="form-control @error('noAfiliate') is-invalid @enderror" name="noAfiliate" value="{{ old('noAfiliate') }}" required autocomplete="noAfiliate" autofocus>

                                @error('noAfiliate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right txtlabel">Contraseña</label>

                            <div class="col-md-6 labelinput">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4 labelinput">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 footlogin labelinput">
                                <button type="submit" class="btn btn-primarylogin">
                                    ENTRAR
                                </button>
                                <br>
                                <p></p>
                                <span>No puedes acceder a tu cuenta?<br>
                                Llamanos al 5555555555<br>
                                y te ayudamos</span>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
