@extends('Front.mainjuegom')

@section('content')
    <div class="container">


        <section class="score-panel">
            <ul class="stars">
                <li><i class="fa fa-star"></i></li>
                <li><i class="fa fa-star"></i></li>
                <li><i class="fa fa-star"></i></li>
            </ul>

            <span class="moves">0</span>Movimiento(s)<div class="timer">
            </div>

            <div class="restart" onclick=startGame()>
                <i class="fa fa-repeat"></i>
            </div>
        </section>

        <ul class="deck" id="card-deck">
            <li class="card" type="diamond">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem1.jpg')}}">
                </div>
            </li>
            <li class="card match" type="anchor">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem2.jpg')}}">
                </div>
            </li>
            <li class="card" type="bolt" >
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem3.jpg')}}">
                </div>
            </li>
            <li class="card match" type="anchor">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem2.jpg')}}">
                </div>
            </li>
            <li class="card" type="leaf">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem4.jpg')}}">
                </div>
            </li>
            <li class="card" type="bicycle">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem5.jpg')}}">
                </div>
            </li>
            <li class="card" type="diamond">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem1.jpg')}}">
                </div>
            </li>
            <li class="card" type="bomb">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem6.jpg')}}">
                </div>
            </li>
            <li class="card" type="leaf">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem4.jpg')}}">
                </div>
            </li>
            <li class="card" type="bomb">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem6.jpg')}}">
                </div>
            </li>
            <li class="card open show" type="bolt">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem3.jpg')}}">
                </div>
            </li>
            <li class="card" type="bicycle">
                <div class="bodycard">
                    <img class="imgBodyCard" src="{{ asset('img/memoria/mem5.jpg')}}">
                </div>
            </li>
        </ul>

        <div id="popup1" class="overlay">
            <div class="popup">
                <!--<h2>Felicidades 🎉</h2>-->
                <a class="close" href=# >×</a>
                <div class="content-1">
                    ¡ FELICIDADES GANASTE ! <!--🎉🎉-->
                </div>
                <div class="content-2">
                    <p>Hiciste <span id=finalMove> </span> movimientos </p>
                    <p>en <span id=totalTime> </span> </p>
                    <p><!--Rating:-->  <span id=starRating></span></p>
                </div>
                <button id="play-again"onclick="playAgain()">
                    Juega de nuevo 😄</a>
                </button>
            </div>
        </div>


    </div>
@endsection