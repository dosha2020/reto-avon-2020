<nav class="menu">
	<div class="container menuContainer">
		<a class="linkLogo" href="/">
			<img class="logoMenu" src="{{ asset('img/avon.png')}}" alt="">
		</a>
		<ul class="menuList">
			<li class="menuItem condicionesmenu">
                <a class="link" href="{{ url('terminos-y-condiciones') }}">
					Términos y Condiciones
				</a>
			</li>
<!--			<li class="menuItem login">
                <a class="link" href="{{ url('login') }}">
					Login
				</a>
			</li>-->
                        @guest
                            <li class="menuItem login nav-item">
                                <a class="nav-link link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="menuItem login nav-item dropdown">
                                <a id="navbarDropdown" class="link nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->noAfiliate }} <span class="caret"></span>
                                </a>

                                <div class="salir dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="link dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest

		</ul>
	</div>
</nav>