<nav class="menulogin">
	<div class="container menuContainerlogin">
		<a class="linkLogo" href="/">
			<img class="logoMenulogin" src="{{ asset('img/avon.png')}}" alt="">
		</a>
		<ul class="menuList">
			<li class="menuItem">
                <a class="link" href="{{ url('terminos-y-condiciones') }}">
					Términos y Condiciones
				</a>
			</li>
		</ul>
	</div>
</nav>