@extends('Front.main')

@section('content')
    <div class="container-fluid instructionsComponent">
        <div class="container">
            <div class="row mb-5 mt-5 titles1">
                <div class="col-md-12 center mt-5">
                    <h1 class="title-pink upper">Bases de la Promoción</h1>
                </div>
                <div class="col-md-12 center mt-4">
                    <h2 class="title-black upper">Sigue estos simples pasos y gana<br>
                     increíbles premios.</h2>
                </div>
            </div>
            <div class="row mt-5 titles2">
                <div class="col-md-3 col-sm-12 mt-2">
                    <div class="avon-instruction-item">
                        <span class="background-pink point1">1</span>
                        <p class="mt-3">Genera tu Usuario en la página, para esto necesitas:</p><p class="p1">* Registro Avon</p><p class="p1">* Clave asignada, ésta la podras encontrar en tus mensajes de incentivos de tu Factura</p><p class="p1">* Contraseña generada</p></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mt-2">
                    <div class="avon-instruction-item">
                        <span class="background-pink point2">2</span>
                        <p class="mt-3">Ingresas la cantidad de cajillas de la fragancia "Far Away" en la presentación de 50 ml que calcules que caben dentro del auto.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mt-2">
                    <div class="avon-instruction-item">
                        <span class="background-pink point3">3</span>
                        <p class="mt-3">Das clic en enviar. Obtienes un mensaje de confirmación de participación dentro de la misma página web.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 mt-2">
                    <div class="avon-instruction-item">
                        <span class="background-pink point4">4</span>
                        <p class="mt-3">Por cada oportunidad de participar que tenga el usuario podrá ingresar un número de cajillas de fragancia “Far Away” 50 ml que calcule que están dentro de la cabina de conductor, copiloto y pasajeros del automóvil ya mencionado, sin tomar en cuenta la cajuela y la guantera.</p>
                    </div>
                </div>
           </div>
            <div class="row mt-5 mb-5 justify-content-center terminoscon">
                <a href="{{ url('terminos-y-condiciones') }}" class="router-link">
                    <div class="upper btn-pink btn-cta-terms box-shadow-pink">
                        <span class="upper">Términos y Condiciones</span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="background-grey container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center mt-5 ">
                    <div class="avon-cta-login">
                        <h2 class="title-2 upper">
                            Regístrate y participa para<br> ganar un auto último modelo
                        </h2>
                        
                    </div>
                </div>
                <div class="col-md-12 center mb-5">
                    <!-- <router-link to="/memoria" class="router-link">
                        <div class="upper btn-pink btn-cta box-shadow-pink">
                            <span class="upper">puedes participar a partir del 25 de Enero 2021 a las 00:00</span>
                        </div>
                    </router-link> -->
                    <h2 class="title-2 upper titles-2">
                            Puedes participar a partir <br> Del 25 de Enero 2021 a las 00:00
                        </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cardSliderComponent">
        <div class="container mt-5 bt-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider1">
                        <div class="itemSlider1">    
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/auto/auto-final-4.jpg')}}" />
                            </div>
                        </div>
                        <div class="itemSlider1">
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/auto/auto-final-3.jpg')}}" />
                            </div>
                        </div>
                        <div class="itemSlider1">
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/auto/auto-final-2.jpg')}}" />
                            </div>
                        </div>
                        <div class="itemSlider1">
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/auto/auto-final-1.jpg')}}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid gamesButtonsComponent">
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-6 col-sm-12 center mt-2">
                     <a href="{{ url('juego-gato') }}" class="router-link"> 
                        <div class="btn-pink btn-game box-shadow-pink upper">
                            <img src="{{ asset('img/cat.png')}}" class="img-fluid" />
                            <span class="upper">Gato</span>
                        </div>
                     </a>
                </div>
                <div class="col-md-6 col-sm-12 center mt-2">
                     <a href="{{ url('juego-memoria') }}" class="router-link"> 
                        <div class="btn-pink btn-game box-shadow-pink upper">
                            <img src="{{ asset('img/memo.png')}}" class="img-fluid" />
                            <span class="upper">Memoria</span>
                        </div>
                     </a> 
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container mt-3">
            <div class="row">
                <div class="col-md-12">
                    <div class="videoslider1">
                        <div class="itemSliderVideo">    
                            <div class="avon-video box-shadow-black" >
                                <video src="{{ asset('video/bienvenida.mp4')}}" controls  poster="{{ asset('img/video-1-poster.jpg')}}"></video>
                                <div class="video-caption">
                                    <h2 class="title-black">Bienvenida</h2>
                                    <!--<h4 class="title-grey">Descripción</h4>-->
                                </div>
                            </div>
                        </div>
                        <!-- <div class="itemSliderVideo">
                            <div class="avon-video box-shadow-black" >
                                <video src="{{ asset('video/avon_cuidarte.mp4')}}" controls poster="{{ asset('img/video-2-poster.jpg')}}"></video>
                                <div class="video-caption">
                                    <h2 class="title-black">Avon para Cuidarte</h2>
                                    <h4 class="title-grey">Descripción</h4>
                                </div>
                            </div>
                        </div>
                        <div class="itemSliderVideo">
                            <div class="avon-video box-shadow-black" >
                                <video src="{{ asset('video/lieas_productos.mp4')}}" controls poster="{{ asset('img/video-3-poster.jpg')}}"></video>
                                <div class="video-caption">
                                    <h2 class="title-black">Lineas de Producto</h2>
                                    <h4 class="title-grey">Descripción</h4>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection