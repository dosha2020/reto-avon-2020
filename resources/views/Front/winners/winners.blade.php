@extends('Front.mainothers')
 
@section('content')
<div class="containerWinners">
    <div class="container-fluid">
        <div class="container mt-5 bt-5">
            <div class="bodyContainerMessage">
                <p class="titleWinenrs">Felicidades A:</p>
                <p class="nameWinners">Enrique Armando Guasp Lopez</p>
                <p class="textWinners">Ganador de un automóvil</p>
                <p class="textWinners">Chevrolet Beat Notchback LS 2021</p>
            </div>
        </div>
    </div>
    <div class="container-fluid cardSliderComponent">
        <div class="container mt-5 bt-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="flexslider1">
                        <div class="itemSlider1">    
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/winners/winners-4.jpg')}}" />
                            </div>
                        </div>
                        <div class="itemSlider1">
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/winners/winners-3.jpg')}}" />
                            </div>
                        </div>
                        <div class="itemSlider1">
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/winners/winners-2.jpg')}}" />
                            </div>
                        </div>
                        <div class="itemSlider1">
                            <div class="avon-car box-shadow-black">
                                <img class="img-fluid" src="{{ asset('img/winners/winners-1.jpg')}}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
