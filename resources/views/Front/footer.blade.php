<footer class="container-fluid">
    <div class="containers">
        <div class="row">
            <div class="col-md-12">
                <div class="center" id="avon-footer">
                    <p>
                    PROFECO: 5438/2020 | 
                        <a class="link" href="{{ url('terminos-y-condiciones') }}">Términos y Condiciones</a>
                     | © Avon 2020
                    </p>
                </div>    
            </div>
        </div>
    </div>
</footer>