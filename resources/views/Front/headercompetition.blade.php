<div class="container-fluid mb-5 headerComponent" >
    <div id="avon-elipse"></div>
    <div class="container pt-5">
        <div class="row mb-5">
            <div class="col-md-6">
                <!-- <div id="avon-logo"></div> -->
            </div>
            <div class="col-md-6">
                <!-- <div id="avon-dice"></div> -->
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-6 col-sm-12 auto">
                <div id="avon-car-teaser">
                    <img class="img-fluid" src="{{ asset('img/auto-teaser.png')}}" />
                </div>
            </div>
            <div class="col-md-6 col-sm-12 gana">
                <div id="avon-bet-logo center" style="text-align:center">
                    <img class="img-fluid img-fluid2" src="{{ asset('img/reto-avon.png')}}" />
                    <h1 style="margin: 30px;" class="upper title-white">¡Gana un Auto <br> último modelo!</h1>
                </div>
            </div>
        </div>
    </div>
</div>