<nav class="menu">
	<div class="container menuContainer">
		<a class="linkLogo" href="/">
			<img class="logoMenu" src="{{ asset('img/avon.png')}}" alt="">
		</a>
		<ul class="menuList">
			<li class="menuItem condicionesmenu">
                <a class="link" href="{{ url('terminos-y-condiciones') }}">
					Términos y Condiciones
				</a>
			</li>
			@if ($stage === '1')
			<li class="menuItem login">
                <a class="link" href="{{ url('login') }}">
					Login
				</a>
			</li>
			@endif
		</ul>
	</div>
</nav>