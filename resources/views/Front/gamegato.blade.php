@extends('Front.mainjuego')

@section('content')

            <div class="center-text">
                <h3>Elige una opción:</h3>
                <a class="btn-pink option btn-choose" data-option="true">X</a>
                <a class="btn-pink option btn-choose" data-option="false">O</a>
            </div>

        <div id="main">


            <div class="col-12">
                <div class="row">
                    <div class="col-xs-4 cell" id="c11">
                    </div>
                    <div class="col-xs-4 cell" id="c12">
                    </div>
                    <div class="col-xs-4 cell" id="c13">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 cell" id="c21">
                    </div>
                    <div class="col-xs-4 cell" id="c22">
                    </div>
                    <div class="col-xs-4 cell" id="c23">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4 cell" id="c31">
                    </div>
                    <div class="col-xs-4 cell" id="c32">
                    </div>
                    <div class="col-xs-4 cell" id="c33">
                    </div>
                </div>
            </div>
        </div>
         <div id="popup1" class="overlay">
            <div class="popup">
                <!--<h2>Felicidades 🎉</h2>-->
                <a class="close" href=# >×</a>
                <div class="content-1">
                    ¡ Perdiste mejor suerte para la próxima ! <!--🎉🎉-->
                </div>
                <div class="content-2" style="display: none;">
                    <p>Hiciste <span id=finalMove> </span> movimientos </p>
                    <p>en <span id=totalTime> </span> </p>
                    <p><!--Rating:-->  <span id=starRating></span></p>
                </div>
                <button id="play-again"onclick="playAgain()" style="display: none;">
                    Juega de nuevo 😄</a>
                </button>
            </div>
        </div>

        <div id="popup2" class="overlay2">
            <div class="popup2">
                <!--<h2>Felicidades 🎉</h2>-->
                <a class="close2" href=# >×</a>
                <div class="content-1">
                    ¡ FELICIDADES GANASTE ! <!--🎉🎉-->
                </div>
                <div class="content-2" style="display: none;">
                    <p>Hiciste <span id=finalMove> </span> movimientos </p>
                    <p>en <span id=totalTime> </span> </p>
                    <p><!--Rating:-->  <span id=starRating></span></p>
                </div>
                <button id="play-again"onclick="playAgain()" style="display: none;">
                    Juega de nuevo 😄</a>
                </button>
            </div>
        </div>

        <div id="popup3" class="overlay3">
            <div class="popup3">
                <!--<h2>Felicidades 🎉</h2>-->
                <a class="close3" href=# >×</a>
                <div class="content-1">
                    ¡ EMPATASTE INTENTA DE NUEVO ! <!--🎉🎉-->
                </div>
                <div class="content-2" style="display: none;">
                    <p>Hiciste <span id=finalMove> </span> movimientos </p>
                    <p>en <span id=totalTime> </span> </p>
                    <p><!--Rating:-->  <span id=starRating></span></p>
                </div>
                <button id="play-again"onclick="playAgain()" style="display: none;">
                    Juega de nuevo 😄</a>
                </button>
            </div>
        </div>

  
@endsection