<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="email=no"> 
    <meta name="format-detection" content="address=no">
    <title>Avon | Gato</title>
    <!-- Styles -->
    <!-- Fonts -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app3.css') }}">
    <link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Coda">
    <link rel="stylesheet prefetch" href="https://fonts.googleapis.com/css?family=Gloria+Hallelujah|Permanent+Marker" >

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-180840475-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());
 gtag('config', 'UA-180840475-1');
</script>

</head>
<body id="body" data-page="{{$page}}" data-lang="{{$lang}}">

        @include('Front.headerjuego')
        @yield('content')

    @include('Front.footer')

        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src='https://code.jquery.com/ui/1.11.4/jquery-ui.js'></script>
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{ mix('js/app3.js') }}"></script>
    <script src="{{ mix('js/all.js') }}"></script>
</body>
</html>
