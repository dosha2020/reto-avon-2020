@extends('Front.mainothers')
 
@section('content')
<div class="containerCommingSoon">
    <div class="container-fluid">
        <div class="container mt-5 bt-5">
            <div class="bodyContainerMessage">
                <p class="titleCommingSoon">¡Muchas Gracias Por Particpar!</p>
                <p class="textCommingSoon">Muy pronto anunciaremos a los ganadores.</p>
            </div>
        </div>
    </div>
    <div class="container-fluid gamesButtonsComponent">
        <div class="col-md-12 mt-5">
            <h2 class="titleGame">Diviertete Mientras <br> Esperas Los Resultados</h2>
        </div>
        <div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-6 col-sm-12 center mt-2">
                     <a href="{{ url('juego-gato') }}" class="router-link"> 
                        <div class="btn-pink btn-game box-shadow-pink upper">
                            <img src="{{ asset('img/cat.png')}}" class="img-fluid" />
                            <span class="upper">Gato</span>
                        </div>
                     </a>
                </div>
                <div class="col-md-6 col-sm-12 center mt-2">
                     <a href="{{ url('juego-memoria') }}" class="router-link"> 
                        <div class="btn-pink btn-game box-shadow-pink upper">
                            <img src="{{ asset('img/memo.png')}}" class="img-fluid" />
                            <span class="upper">Memoria</span>
                        </div>
                     </a> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
