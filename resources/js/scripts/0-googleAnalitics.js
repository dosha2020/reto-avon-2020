// caragr fuente de la pagina Roboto

var bodyTag = document.querySelector('#body');
var page = bodyTag.getAttribute("data-page");
var lang = bodyTag.getAttribute("data-lang");
var browser = bodyTag.getAttribute("data-browser");

WebFontConfig = {
	google: { families: [ 'Montserrat:400,500,700','Quicksand:400,600,700:sans-serif'] }
};
(function() {
	var wf = document.createElement('script');
	wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	wf.type = 'text/javascript';
	wf.async = 'true';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(wf, s);
})(); 

// iniciar google analitics


window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date()); 
gtag('config', 'UA-102054877-3');