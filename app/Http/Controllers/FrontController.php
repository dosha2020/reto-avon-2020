<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $stage = config('stage.stage');
        $data = [
            'page' => 'home',
            'year' => date("Y"),
            'lang' => 'ES',
            'stage' => $stage,
        ];
        return view('Front.index')->with($data);
    }

    public function terms()
    {
        $stage = config('stage.stage');
        $data = [
            'page' => 'terms',
            'year' => date("Y"),
            'lang' => 'ES',
            'stage' => $stage,
        ];
        return view('Front.terms')->with($data);
    }

    public function gamecat()
    {
        $stage = config('stage.stage');
        $data = [
            'page' => 'juego-gato',
            'year' => date("Y"),
            'lang' => 'ES',
            'stage' => $stage,
        ];
        return view('Front.gamegato')->with($data);
    }
    public function gamememory()
    {
        $stage = config('stage.stage');
        $data = [
            'page' => 'juego-memoria',
            'year' => date("Y"),
            'lang' => 'ES',
            'stage' => $stage,
        ];
         return view('Front.gamememoria')->with($data);
    }
    public function winners()
    {
        $stage = config('stage.stage');
        $data = [
            'page' => 'winners',
            'year' => date("Y"),
            'lang' => 'ES',
            'stage' => $stage,
        ];
         return view('Front.winners.winners')->with($data);
    }
    public function comingSoon()
    {
        $stage = config('stage.stage');
        $data = [
            'page' => 'comingSoon',
            'year' => date("Y"),
            'lang' => 'ES',
            'stage' => $stage,
        ];
         return view('Front.comingSoon.comingSoon')->with($data);
    }
 
}
